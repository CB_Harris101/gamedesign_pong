using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreLine : MonoBehaviour
{
    public GameObject controller;
    private controler gameController;

    public AudioClip scoreSound;
    private AudioSource scoreSource;

    void Start()
    {
        gameController = controller.GetComponent<controler>();
        if (scoreSound != null)
        {
            scoreSource = gameObject.AddComponent<AudioSource>();
            scoreSource.clip = scoreSound;
            scoreSource.loop = false;
            scoreSource.playOnAwake = false;
        }
    }

    public void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "ball")
        {
            // play audio effect
            if (scoreSound != null)
                if (scoreSound != null) scoreSource.Play();

            Color ball = GameObject.Find("ball").GetComponent<Renderer>().material.color;

            Color Paddle1Color = GameObject.Find("Paddle1").GetComponent<Renderer>().material.color;
            Color Paddle2Color = GameObject.Find("Paddle2").GetComponent<Renderer>().material.color;
            Color Paddle3Color = GameObject.Find("Paddle3").GetComponent<Renderer>().material.color;
            Color Paddle4Color = GameObject.Find("Paddle4").GetComponent<Renderer>().material.color;

            if (Paddle1Color.Equals(ball) || Color.grey.Equals(ball))
            {
                gameController.UpdateScoredown1();
            }
            if (Paddle2Color.Equals(ball))
            {
                gameController.UpdateScore2();
            }

            if (Paddle3Color.Equals(ball))
            {
                gameController.UpdateScore3();
            }

            if (Paddle4Color.Equals(ball))
            {
                gameController.UpdateScore4();
            }
        }
    }
}
