﻿ using UnityEngine;
using System.Collections;

public class force: MonoBehaviour 
{
	private Rigidbody body;
	public bool randomForce = true;
	public float velocity = 10f;

	void Awake () 
	{
		body = GetComponent<Rigidbody>();
		body.AddForce(new Vector3(25, 0, 25), ForceMode.Impulse);
		
	}

	void OnCollisionEnter(Collision col)
	{
		Vector3 reflection = body.velocity.normalized + ( 2 * (Vector3.Dot(body.velocity.normalized, col.contacts[0].normal)) * col.contacts[0].normal);
		body.velocity = reflection * velocity;



	}

}
