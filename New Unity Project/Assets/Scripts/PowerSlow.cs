using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSlow : MonoBehaviour { 
 public float multiplier = 0.5f;

public GameObject slowPowerup;

void OnTriggerEnter(Collider Other)
{
    if (Other.CompareTag("ball"))
    {
        PowerupSlow(Other);

    }
}

// Update is called once per frame
//void PowerupSlow(Collider ball)
       IEnumerator PowerupSlow(Collider ball)
{
       // ball.transform.localScale *= multiplier;
        ballMovment speed = ball.GetComponent<ballMovment>();
        speed.velocity *= multiplier;

        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;

        //wait a few seconds
        yield return new WaitForSeconds(4f);

        // reverse effect
      //  speed.velocity /= multiplier;

    Destroy(gameObject);
}
}
