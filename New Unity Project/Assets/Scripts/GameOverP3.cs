using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverP3 : MonoBehaviour
{
	public Text score3Text;

	void Awake()
	{
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true;
		score3Text.text = "Score - " + PlayerPrefs.GetInt("Score").ToString();
	}
	public void PlayGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
	}

	public void QuitGame()
	{
		Application.Quit();
	}

}