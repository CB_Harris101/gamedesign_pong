using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class controler : MonoBehaviour
{
    private Transform P1;
    private Transform P2;
    private Transform P3;
    private Transform P4;

    static private int score1 = 0;
    static private int score2 = 0;
    static private int score3 = 0;
    static private int score4 = 0;

    public float maxTime = 120;
    public float sceneTime = 0;

    public Text score1Text;
    private string score1Output = "P1 Score: ";

    public Text score2Text;
    private string score2Output = "P2 Score: ";

    public Text score3Text;
    private string score3Output = "P3 Score: ";

    public Text score4Text;
    private string score4Output = "P4 Score: ";

    public Text timerText;
    private string timeOutput = "Timer: ";

    public GameObject ball;
    public string GameOverScene1;
    public string GameOverP2;
    public string GameOverP3;
    public string GameOverP4;
    
    private bool tenS = true;
    private bool nineS = true;
    private bool eightS = true;
    private bool sevenS = true;
    private bool sixS = true;
    private bool fiveS = true;
    private bool fourS = true;
    private bool threeS = true;
    private bool twoS = true;
    private bool oneS = true;
    private bool zeroS = true;
    private bool fivePS = true;
    private bool fourPS = true;
    private bool threePS = true;
    private bool twoPS = true;
    private bool onePS = true;
    private bool twoPPS = true;
    private bool onePPS = true;
    private bool zeroPPS = true;
    private bool zeroPSS = true;
    private bool onePSS = true;

    void Awake()
    {
        score1 = 0;
        score2 = 0;
        score3 = 0;
        score4 = 0;
        maxTime = 40;

        score1Text.text = score1Output + score1;
        score2Text.text = score2Output + score2;
        score3Text.text = score3Output + score3;
        score4Text.text = score4Output + score4;

        P1 = GameObject.Find("Paddle1").transform;
        P2 = GameObject.Find("Paddle2").transform;
        P3 = GameObject.Find("Paddle3").transform;
        P4 = GameObject.Find("Paddle4").transform;

    }

    void Reset() {
        ball.transform.position = new Vector3(0, 2, 0);
        ball.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        ball.GetComponent<ballMovment>().update();
        //P1.GetComponent<Paddle>().speedReset();
        //P2.GetComponent<paddle2>().speedReset();
        //P3.GetComponent<paddle3>().speedReset();
        //P4.GetComponent<Paddle4>().speedReset();
    }

    void Update()
    {
        sceneTime += Time.deltaTime;
        UpdateTimer();
        ball.GetComponent<ballMovment>().speedUp();
        ball.GetComponent<ballMovment>().Pause();

        if (maxTime - sceneTime <= 0){
            Win();
        }

        colourChanger();
    }

    void colourChanger(){
        if (maxTime - sceneTime <= 10.5 && tenS == true){
            colourChange();
            tenS = false;
        }
        if (maxTime - sceneTime <= 9.5 && nineS == true){
            colourChange();
            nineS = false;
        }
        if (maxTime - sceneTime <= 8.5 && eightS == true){
            colourChange();
            eightS = false;
        }
        if (maxTime - sceneTime <= 7.5 && sevenS == true){
            colourChange();
            sevenS = false;
        }
        if (maxTime - sceneTime <= 6.5 && sixS == true){
            colourChange();
            sixS = false;
        }
        if (maxTime - sceneTime <= 5.5 && fiveS == true){
            colourChange();
            fiveS = false;
        }
        if (maxTime - sceneTime <= 5 && fivePS == true){
            colourChange();
            fivePS = false;
        }
        if (maxTime - sceneTime <= 4.5 && fourS == true){
            colourChange();
            fourS = false;
        }
        if (maxTime - sceneTime <= 4 && fourPS == true){
            colourChange();
            fourPS = false;
        }
        if (maxTime - sceneTime <= 3.5 && threeS == true){
            colourChange();
            threeS = false;
        }
        if (maxTime - sceneTime <= 3 && threePS == true){
            colourChange();
            threePS = false;
        }
        if (maxTime - sceneTime <= 2.5 && twoS == true){
            colourChange();
            twoS = false;
        }
        if (maxTime - sceneTime <= 2.25 && twoPPS == true){
            colourChange();
            twoPPS = false;
        }
        if (maxTime - sceneTime <= 2 && twoPS == true){
            colourChange();
            twoPS = false;
        }
        if (maxTime - sceneTime <= 1.75 && onePSS == true){
            colourChange();
            onePSS = false;
        }
        if (maxTime - sceneTime <= 1.5 && oneS == true){
            colourChange();
            oneS = false;
        }
        if (maxTime - sceneTime <= 1.25 && onePPS == true){
            colourChange();
            onePPS = false;
        }
        if (maxTime - sceneTime <= 1 && onePS == true){
            colourChange();
            onePS = false;
        }
        if (maxTime - sceneTime <= 0.75 && zeroS == true){
            colourChange();
            zeroS = false;
        }
        if (maxTime - sceneTime <= 0.5 && zeroPSS == true){
            colourChange();
            zeroPSS = false;
        }
        if (maxTime - sceneTime <= 0.25 && zeroPPS == true){
            colourChange();
            zeroPPS = false;
        }
    }

    void colourChange(){
        P1.GetComponent<Paddle>().colourChange();
        P2.GetComponent<paddle2>().colourChange();
        P3.GetComponent<paddle3>().colourChange();
        P4.GetComponent<Paddle4>().colourChange();
    }

    public void UpdateScore1()
    {
        score1++;

        score1Text.text = score1Output + score1;

        Reset();
    }
    public void UpdateScoredown1()
    {
        score1--;

        score1Text.text = score1Output + score1;

        Reset();
    }
    public void UpdateScore2()
    {
        score2++;

        score2Text.text = score2Output + score2;

        Reset();
    }
    public void UpdateScoredown2()
    {
        score2--;

        score2Text.text = score2Output + score2;

        Reset();
    }
    public void UpdateScore3()
    {
        score3++;

        score3Text.text = score3Output + score3;

        Reset();
    }
    public void UpdateScoredown3()
    {
        score3--;

        score3Text.text = score3Output + score3;

        Reset();
    }
    public void UpdateScore4()
    {
        score4++;

        score4Text.text = score4Output + score4;

        Reset();
    }
    public void UpdateScoredown4()
    {
        score4--;

        score4Text.text = score4Output + score4;

        Reset();
    }
    
    public void Win(){
        if (score1 > score2){
            if (score1 > score3){
                if (score1 > score4){
                    Debug.Log("P1Wins");
                    GameOver(GameOverScene1);
                }
                else{
                    Debug.Log("P4Wins");
                    GameOver(GameOverP4);
                }
            }
            else if (score3 > score4){
                Debug.Log("P3Wins");
                GameOver(GameOverP3);
            }
        }
        else if (score2 > score3){
            if (score2>score4){
                Debug.Log("P2Wins");
                GameOver(GameOverP2);
            }
            else{
                Debug.Log("P4Wins");
                GameOver(GameOverP4);
            }
        }
        else if (score3 > score4){
            Debug.Log("P3Wins");
            GameOver(GameOverP3);
        }
        else{
            Debug.Log("P4Wins");
            GameOver(GameOverP4);
        }
    }

	void UpdateTimer()
	{
		timerText.text = timeOutput + (Mathf.RoundToInt(maxTime - sceneTime)).ToString();
	}

	void GameOver(string levelName)
    {
		PlayerPrefs.SetInt("Score", score1);
		SceneManager.LoadScene(levelName);
    }

}
