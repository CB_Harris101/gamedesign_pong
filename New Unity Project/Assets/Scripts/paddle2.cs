using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paddle2 : MonoBehaviour
{
    private float speed;
    string input;

    [SerializeField] private Material myMaterial;
    public AudioClip bounceSound;
    private AudioSource bounceSource;

    void Start()
    {
        speed = 20f;
        input = "P2";
        if (bounceSound != null)
        {
            bounceSource = gameObject.AddComponent<AudioSource>();
            bounceSource.clip = bounceSound;
            bounceSource.loop = false;
            bounceSource.playOnAwake = false;
        }
    
}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis(input) > 0 && transform.position.x > -20.4 || Input.GetAxis(input) < 0 && transform.position.x < 20.4){
            float move = Input.GetAxis(input) * Time.deltaTime * speed;
            transform.Translate(move * Vector3.left);
        }
        speedUp();
    }

    public void speedUp(){
		speed = speed * 1.0002f;
	}

    public void speedReset(){
        speed = 20f;
    }

    public void colourChange(){
        Color32 temp = gameObject.GetComponent<Renderer>().material.color;
        byte rR = (byte)(Random.Range(0,255));
        byte rG = (byte)(Random.Range(0,255));
        byte rB = (byte)(Random.Range(0,255));
        Color32 randc = new Color32(rR,rG,rB,255);
        GameObject.Find("Bottom Left").GetComponent<Renderer>().material.color = randc;
        gameObject.GetComponent<Renderer>().material.color = randc;
        if (myMaterial.color == temp){
            myMaterial.color = randc;
        }
    }

    private void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "ball")
        {
            if (bounceSound != null)
                if (bounceSound != null) bounceSource.Play();
            Debug.Log("Collision");


            myMaterial.color = gameObject.GetComponent<Renderer>().material.color;
            //colourChange();
        }
    }
}

