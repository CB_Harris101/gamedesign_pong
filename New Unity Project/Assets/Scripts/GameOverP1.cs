using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverP1 : MonoBehaviour
{
	public Text scoreText;

	void Awake()
    {
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true;

	}
	public void PlayGame()
	{
		SceneManager.LoadScene("WLP_Scene",LoadSceneMode.Single);
	}

	public void QuitGame()
	{
		Application.Quit();
	}

}
