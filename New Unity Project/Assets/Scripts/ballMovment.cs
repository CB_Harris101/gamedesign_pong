﻿using UnityEngine;
using System.Collections;

public class ballMovment: MonoBehaviour 
{
	private Rigidbody body;
	public bool randomForce = true;
	public float velocity = 10f;

	public GameObject controller;
	private controler gameController;

    public AudioClip bounceSound;
    private AudioSource bounceSource;

    [SerializeField] private Material myMaterial;

	public Material Material1;
	public GameObject Object;
	public int pause = 0;
	private bool heh = false;

	void Awake () 
	{
		gameController = controller.GetComponent<controler>();
		body = gameObject.GetComponent<Rigidbody>();
		pause = 100;
		myMaterial.color = Color.grey;

        if (bounceSound != null)
        {
            bounceSource = gameObject.AddComponent<AudioSource>();
            bounceSource.clip = bounceSound;
            bounceSource.loop = false;
            bounceSource.playOnAwake = false;
        }
    }

	public void Pause(){
		pause -= 1;
		if (pause == 0){
			RandDirection();
			heh = true;
		}
		if (pause == -2 && heh == true || pause == -3 && heh == true){
			body.velocity = body.velocity * Mathf.Pow(1.0002f, gameController.sceneTime*120);
			heh = false;
		}
	}

	public void RandDirection()
	{
		int xv = Random.Range(-40, 40);
		int xvp = 40 - Mathf.Abs(xv);
		int yx = Random.Range(0, 2);
		if (yx.Equals(0))
		{
			body.AddForce(new Vector3(xv, 0, xvp), ForceMode.Impulse);
		}
		else
		{
			body.AddForce(new Vector3(xv, 0, -xvp), ForceMode.Impulse);
		}
	}

    void OnCollisionEnter(Collision col)
    {
         if (col.gameObject.tag == "wall")
            // play audio effect
            if (bounceSound != null)
                if (bounceSound != null) bounceSource.Play();
    }

	public void update ()
    {
		Object.GetComponent<MeshRenderer>().material = Material1;
		myMaterial.color = Color.grey;
		pause = 50;
	}

	public void speedUp(){
		body.velocity = body.velocity * 1.0004f;
	}
}
